# GOSH Website Project

This is a repository for improvements to the GOSH web resources (inc forum, website, gitlab).

We are calling for community members to add and discuss suggested tasks in the [Issues section](https://gitlab.com/gosh-community/gosh-website/-/issues). For more information check [this forum post](https://forum.openhardware.science/t/gosh-web-improvements-call-for-contributors-and-coordinator/2471).

### What to do
- Head to the [Issues section](https://gitlab.com/gosh-community/gosh-website/-/issues)
- Read the issues in [list](https://gitlab.com/gosh-community/gosh-website/-/issues) or [board mode](https://gitlab.com/gosh-community/gosh-website/-/boards), comment on any of interest and give a [thumbs up](https://docs.gitlab.com/ee/user/award_emojis.html) to your highest priorities.
- [Add a new issue](https://docs.gitlab.com/ee/user/project/issues/index.html) and apply the appropriate labels e.g. website, forum

Check [the forum post](https://forum.openhardware.science/t/gosh-web-improvements-call-for-contributors-and-coordinator/2471) for meetings to discuss and to volunteer to join or (if you would be willing!) to coordinate the GOSH web working group.

